import React from 'react';
import {Provider} from 'react-redux';
import Content from "./components/content/"
import Store from './redux/store';

function App() {
  return (
    <Provider store={Store}>
      <Content/>
    </Provider>
  );
}

export default App;
