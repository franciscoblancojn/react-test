const defaultState = [
    {
        id:1,
        title:'1'
    },
    {
        id:2,
        title:'2'
    },
    {
        id:3,
        title:'3'
    }
]

function reducer(state = defaultState, { type, payload }){
    switch(type){
        case "add_todo":{
            return [
                ...state,
                payload.add
            ]
        }
        default:
            return state
    }
}
export default reducer