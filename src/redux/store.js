import { createStore} from 'redux';
//combineReducers
// import todo from './reducers/todo';
// import state from './state/state';
const defaultState = {
    debug : true,
    state:{
        active_menu:false
    }
}

const reducer = (state = defaultState, action) =>{
    if(state.debug){
        console.log(action.type);
    }
    switch (action.type) {
        case "toogle_menu":
            return {
                ...state,
                state:{
                    active_menu : !state.state.active_menu
                }
            };
            break;
    
        default:
            break;
    }
    return state;
}

const store = createStore(reducer)

export default store