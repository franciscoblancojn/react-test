const defaultState = {
    active_menu : false,
    toogle_menu : function() {
        this.active_menu = !this.active_menu
    }
}

function reducer(state = defaultState, action){
    switch(action.type){
        case "toogle_menu":{
            state.active_menu = !state.active_menu
        }
        default:
            return state
    }
    return state
}
export default reducer