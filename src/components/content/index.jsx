import React, { Component } from 'react';
import { BrowserRouter, Route, Redirect, Switch , NavLink} from 'react-router-dom';
import Header from '../header/';
import Home from '../home/';
import Search from '../search';
import Login from '../login/';
import {connect} from 'react-redux';

class Content extends Component {
  render() {
    const { debug , state } = this.props
    if(debug){
      console.log(state)
    }
    return (
        <BrowserRouter>
            <Header>
                <NavLink to="/" 
                activeClassName="active" 
                onClick={()=>this.props.toogle_menu()}
                >Home</NavLink>
                <NavLink to="/login" 
                activeClassName="active" 
                onClick={()=>this.props.toogle_menu()}
                >Login</NavLink>
            </Header>
            <Switch>
                <Route path="/react-test/login" component={Login} />
                <Route path="/react-test/home" component={Home} />
                <Route path="/react-test/search" component={Search} />
                <Redirect from="/login" to="/react-test/login" />
                <Redirect from="/" to="/react-test/home" />
            </Switch>
        </BrowserRouter>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    debug: state.debug,
    state: state.state
  }
}
const mapDispatchToProps = dispatch => {
  return{
    toogle_menu: () => dispatch({
                          type:"toogle_menu"
                        })
  }
}

export default connect(mapStateToProps , mapDispatchToProps)(Content)