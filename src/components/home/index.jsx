import React, { Component } from 'react';
import Page from './page';
import {connect} from 'react-redux';

class Home extends Component {
  render() {
    return (
        <Page />
    );
  }
}
const mapStateToProps = (state) => {
  return {
    debug: state.debug,
    todo: state.todo
  }
}

export default connect(mapStateToProps)(Home)