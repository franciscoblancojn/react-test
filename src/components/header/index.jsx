import React, { Component } from 'react';
import Page from './page';
import {connect} from 'react-redux';

class Header extends Component {
  constructor(props) {
      super(props);
      this.state = {
          active: false
      };
      this.toogle_menu = this.toogle_menu.bind(this);
  }
  toogle_menu() {
      // this.setState(state => ({
      // active: !state.active
      // }));
      this.props.toogle_menu()
  }
  render() {
    const { debug , state } = this.props
    if(debug){
      console.log(state)
    }
    return (
      <header className={(state.active_menu)?"active":""}>
        <Page toogle_menu={()=>this.props.toogle_menu()} children={this.props.children}/>
      </header>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    debug: state.debug,
    state: state.state
  }
}
const mapDispatchToProps = dispatch => {
  return{
    toogle_menu: () => dispatch({
                          type:"toogle_menu"
                        })
  }
}

export default connect(mapStateToProps , mapDispatchToProps)(Header)