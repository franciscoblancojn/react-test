import React, { Component } from 'react';

export default class Page_Login extends Component {
  render() {
    return (
        <div className="formLogin">
          <form>
            <label htmlFor="user" className="content_label">
              <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/></svg>
              <div className="content_input">
                <span>User</span>
                <input 
                onChange={this.props.event.onchange}
                type="text" name="user" id="user"/>
              </div>
            </label>
            <label htmlFor="password" className="content_label">
              <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12.65 10C11.83 7.67 9.61 6 7 6c-3.31 0-6 2.69-6 6s2.69 6 6 6c2.61 0 4.83-1.67 5.65-4H17v4h4v-4h2v-4H12.65zM7 14c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"/></svg>
              <div className="content_input">
                <span>Password</span>
                <input 
                onChange={this.props.event.onchange}
                type="password" name="password" id="password"/>
              </div>
            </label>
            <div className="content_btn">
              <a 
              onClick={this.props.event.login}
              className="btn_login">
                Login
              </a>
            </div>
          </form>
        </div>
    );
  }
}
