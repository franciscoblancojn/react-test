import React, { Component } from 'react';
import Page from './page';
import {connect} from 'react-redux';

class Login extends Component {
  constructor(props) {
      super(props);
      this.state = {
          user: "",
          password: ""
      };
  }
  onchange(e){
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  login_api(){
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Cookie", "__cfduid=d23155ce328a4759efd2b35fde15da2211600376510");

    var raw = JSON.stringify({
      "action":"login",
      "data":this.state
    });

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("https://blanconegrotest.startscoinc.com/wp-content/plugins/api_react/api.php", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }
  login(){
    console.log(this.state);
    this.login_api()
  }
  render() {
    const { debug , state } = this.props
    if(debug){
      console.log(state)
    }
    return (
        <Page 
        event={
          {
            "state" : this.state,
            "login": () => this.login(),
            "onchange":(e) => this.onchange(e),
          }
        }
        />
    );
  }
}
const mapStateToProps = (state) => {
  return {
    debug: state.debug,
    state: state.state
  }
}
const mapDispatchToProps = dispatch => {
  return{
  }
}

export default connect(mapStateToProps , mapDispatchToProps)(Login)